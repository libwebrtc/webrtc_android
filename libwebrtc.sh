#!/bin/bash

# This script fetches WebRTC source code using Chromium depot tools,
# checks out a specified branch, installs build dependencies,
# and builds libwebrtc Android AAR libraries for armeabi-v7a, arm64-v8a, and x86 CPUs.

# Usage: ./build_webrtc.sh <branch_name>

set -e  # Exit on error

# Check if the script is being executed from the home directory
if [ "$PWD" != "$HOME" ]; then
    echo "This script must be executed from the home directory"
    exit 1
fi

# Input validation
if [ $# -ne 1 ]; then
    echo "Usage: $0 <branch_name>"
    exit 1
fi

BRANCH="$1"
echo "Checking out branch '$BRANCH'"

# Clone Chromium depot tools if it doesn't exist already
if [ ! -d "$HOME/depot_tools" ]; then
    git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git "$HOME/depot_tools"
fi

# Add Chromium depot tools to the PATH
export PATH="$HOME/depot_tools:$PATH"

# Check if webrtc_android directory already exists
if [ -d "$HOME/webrtc_android" ]; then
    echo "webrtc_android directory already exists. Updating the repository."
    cd "$HOME/webrtc_android/src"

    # Ensure we're on the main branch and pull the latest changes
    git checkout main
    git pull --rebase
else
    # Fetch WebRTC source code
    mkdir -p "$HOME/webrtc_android"
    cd "$HOME/webrtc_android"
    fetch --nohooks webrtc_android  
fi

# Sync the source code to the latest version
cd "$HOME/webrtc_android/src"
gclient sync

# Install dependencies
if [ -x build/install-build-deps.sh ]; then
    ./build/install-build-deps.sh
else
    echo "Dependency installation script not found!"
    exit 1
fi

# List remote branches
echo "Available remote branches:"
git branch -r

# Check if the specified branch exists
if ! git show-ref --verify --quiet "refs/remotes/origin/$BRANCH"; then
    echo "Branch '$BRANCH' does not exist on the remote repository."
    exit 1
fi

# Checkout the specified branch
git checkout "$BRANCH"
gclient sync

# Build libwebrtc AAR for armeabi-v7a, arm64-v8a, and x86
echo "Building libwebrtc AAR for armeabi-v7a, arm64-v8a, and x86"
tools_webrtc/android/build_aar.py \
    --arch=arm,arm64,x86 \
    --target_arch=arm,arm64,x86 \
    --toolchain=clang \
    --extra-gn-args='target_os="android" target_cpu="arm" enable_stripping=true is_debug=false' \
    --output-dir=out/android_libs/

echo "Successfully built libwebrtc AAR libraries."
